(function() {

  function sendHeight(height) {
    window.parent.postMessage(height, '*');
  }

  $(function() {
    sendHeight($('.container')[0].offsetHeight);
    
    $(window).resize(function() {      
      sendHeight($('.container')[0].offsetHeight);
    });
  });
})();