function Mozgva(object) {
  this.wrapper_id = object.id;
  this.src = object.url;
  this.id = 'mozgva_' + new Date().getTime();

  if (!this.src) {
    console.log('Missing url for Mozgva iframe');

    return;
  }

  var wrapper = document.getElementById(this.wrapper_id);

  if (!wrapper) {
    console.error('Missing id wrapper for Mozgva iframe');

    return;
  }

  var iframe = document.createElement('iframe');
  
  iframe.setAttribute('id', this.id);
  iframe.setAttribute('src', this.src);
  iframe.setAttribute('style', 'width: 100%; border: none;');

  wrapper.appendChild(iframe);

  if (window.addEventListener) {
    window.addEventListener("message", listener.bind(this));
  } else {
    window.attachEvent("onmessage", listener.bind(this));
  }
  
  function listener(event) {
    var el = document.getElementById(this.id);

    try {
      var data = JSON.parse(event.data);

      if (data.from === 'mozgva') {
        el.height = data.height + 'px';
      }
    } catch(e) {}
  }
}